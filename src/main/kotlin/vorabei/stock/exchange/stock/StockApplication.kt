package vorabei.stock.exchange.stock

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories
import com.mongodb.reactivestreams.client.MongoClients

import com.mongodb.reactivestreams.client.MongoClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.mongodb.config.EnableMongoAuditing
import org.springframework.data.mongodb.config.EnableReactiveMongoAuditing
import org.springframework.scheduling.annotation.EnableScheduling
import org.axonframework.config.ConfigurationScopeAwareProvider

import org.axonframework.deadline.SimpleDeadlineManager

import org.axonframework.spring.config.AxonConfiguration

import org.axonframework.deadline.DeadlineManager
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope


@SpringBootApplication
@EnableReactiveMongoRepositories
@EnableReactiveMongoAuditing
@EnableScheduling
@Configuration
class StockApplication {
}

fun main(args: Array<String>) {
	SpringApplication.run(StockApplication::class.java, *args)
}
