package vorabei.stock.exchange.stock.api

import org.axonframework.commandhandling.RoutingKey
import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.util.*

class CreateFoodCartCommand(
        @RoutingKey val foodCartId: UUID
)

data class SelectProductCommand(
        @TargetAggregateIdentifier val foodCartId: UUID,
        val productId: UUID,
        val orderId: UUID,
        val quantity: Int
)

data class DeselectProductCommand(
        @TargetAggregateIdentifier val foodCartId: UUID,
        val productId: UUID,
        val quantity: Int
)

data class ConfirmOrderCommand(
        @TargetAggregateIdentifier val foodCartId: UUID
)

data class CreateProductCommand(
        @TargetAggregateIdentifier val productId: UUID,
        val name: String,
        val code: String,
        val quantity: Int
        )
data class BookedProductCommand(
        @TargetAggregateIdentifier val productId: UUID,
        val orderId: UUID,
        val quantity: Int
        )

data class UnBookedProductCommand(
        @TargetAggregateIdentifier val productId: UUID,
        val orderId: UUID,
        val quantity: Int
        )
data class CreateOrderCommand (
        val productId: UUID,
        val foodCartId: UUID,
        @TargetAggregateIdentifier val orderId: UUID,
        val quantity: Int
        )
data class CancelOrderCommand (
        @TargetAggregateIdentifier val orderId: UUID,
        val reasonFailed: String
        )
data class SuccessOrderCommand (
        @TargetAggregateIdentifier val orderId: UUID,
        )