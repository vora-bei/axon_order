package vorabei.stock.exchange.stock.api

enum class OrderStatus {
    PROCESSING,
    SUCCESS,
    FAILED,
}