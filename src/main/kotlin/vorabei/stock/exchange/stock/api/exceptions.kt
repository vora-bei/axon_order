package vorabei.stock.exchange.stock.api

class ProductDeselectionException(message: String) : Exception(message)