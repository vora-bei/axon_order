package vorabei.stock.exchange.stock.api

import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.util.*

class FoodCartCreatedEvent(
        val foodCartId: UUID
)

data class ProductSelectedEvent(
        val foodCartId: UUID,
        val productId: UUID,
        val orderId: UUID,
        val quantity: Int
)

data class ProductDeselectedEvent(
        val foodCartId: UUID,
        val productId: UUID,
        val quantity: Int
)

data class OrderConfirmedEvent(
        val foodCartId: UUID
)

data class CreateProductEvent(
        @TargetAggregateIdentifier val productId: UUID,
        val name: String,
        val code: String,
        val quantity: Int
        )
data class BookedProductEvent(
        @TargetAggregateIdentifier val productId: UUID,
        val orderId: UUID,
        val quantity: Int
        )
data class UnBookedProductEvent(
        @TargetAggregateIdentifier val productId: UUID,
        val orderId: UUID,
        val quantity: Int
        )
data class BookedFoodCartEvent(
        @TargetAggregateIdentifier val productId: UUID,
        val orderId: UUID,
        val quantity: Int
        )
data class CreateOrderEvent(
        @TargetAggregateIdentifier val orderId: UUID,
        val foodCartId: UUID,
        val productId: UUID,
        val quantity: Int
        )


data class CancelOrderEvent(
        @TargetAggregateIdentifier val orderId: UUID,
        val reasonFailed: String
)
data class SuccessOrderEvent (
        @TargetAggregateIdentifier val orderId: UUID,
)


