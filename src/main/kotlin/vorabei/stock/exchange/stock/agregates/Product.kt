package vorabei.stock.exchange.stock.agregates

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.commandhandling.RoutingKey
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate
import vorabei.stock.exchange.stock.api.*
import java.util.*

@Aggregate
class Product {

    @AggregateIdentifier
    private lateinit var productId: UUID;
    private var quantity: Int = 0
    private lateinit var name: String
    @RoutingKey
    private lateinit var code: String
    constructor() {
        // Required by Axon
    }
    @CommandHandler
    constructor(command: CreateProductCommand) {
        AggregateLifecycle.apply(CreateProductEvent(
            command.productId,
            command.name,
            command.code,
            command.quantity,
        ))
    }
    @CommandHandler
    @Throws(ProductDeselectionException::class)
    fun on(command: BookedProductCommand) {
        if(command.quantity <= quantity){
            AggregateLifecycle.apply(
                BookedProductEvent(
                    command.productId,
                    command.orderId,
                    command.quantity,
                ),
            )
        } else {
            throw ProductDeselectionException(
                "Cannot booked products of ID [$productId]  initially"
            )
        }
    }
    @CommandHandler
    fun on(command: UnBookedProductCommand) {
            AggregateLifecycle.apply(
                UnBookedProductEvent(
                    command.productId,
                    command.orderId,
                    command.quantity,
                ),
            )
    }

    @EventSourcingHandler
    fun on(event: CreateProductEvent) {
        productId = event.productId
        name = event.name
        code = event.code
        quantity = event.quantity
    }
    @EventSourcingHandler
    fun on(event: BookedProductEvent) {
        productId = event.productId
        quantity -= event.quantity
    }
    @EventSourcingHandler
    fun on(event: UnBookedProductEvent) {
        productId = event.productId
        quantity += event.quantity
    }


}