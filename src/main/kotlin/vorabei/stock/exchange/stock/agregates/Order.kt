package vorabei.stock.exchange.stock.agregates

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate
import vorabei.stock.exchange.stock.api.*
import java.util.*

@Aggregate
class Order {

    @AggregateIdentifier
    private lateinit var orderId: UUID
    private var quantity: Int = 0
    private lateinit var productId: UUID
    private lateinit var status: OrderStatus
    private var reasonFailed: String? = null;
    constructor() {
        // Required by Axon
    }
    @CommandHandler
    constructor(command: CreateOrderCommand) {

        // @TODO  unique code
        AggregateLifecycle.apply(CreateOrderEvent(
            command.orderId,
            command.foodCartId,
            command.productId,
            command.quantity,
        ))
    }
    @CommandHandler
    fun on(command: CancelOrderCommand) {
        AggregateLifecycle.apply(CancelOrderEvent(
            command.orderId,
            command.reasonFailed
        ))
    }
    @CommandHandler
    fun on(command: SuccessOrderCommand) {
        AggregateLifecycle.apply(SuccessOrderEvent(
            command.orderId
        ))
    }

    @EventSourcingHandler
    fun on(event: CreateOrderEvent) {
        productId = event.productId
        orderId = event.orderId
        quantity = event.quantity
        status = OrderStatus.PROCESSING
    }
    @EventSourcingHandler
    fun on(event: CancelOrderEvent) {
        status = OrderStatus.FAILED
        reasonFailed = event.reasonFailed
    }
    @EventSourcingHandler
    fun on(event: SuccessOrderEvent) {
        status = OrderStatus.SUCCESS
    }


}