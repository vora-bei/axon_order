package vorabei.stock.exchange.stock.agregates

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate
import org.slf4j.LoggerFactory
import vorabei.stock.exchange.stock.api.*
import java.util.*
import kotlin.collections.HashMap

@Aggregate
internal class FoodCart{
    @AggregateIdentifier
    private lateinit var foodCartId: UUID;
    private var selectedProducts: MutableMap<UUID, Int> = mutableMapOf()
    private var confirmed = false

    constructor() {
        // Required by Axon
    }

    @CommandHandler
    constructor(command: CreateFoodCartCommand) {
        AggregateLifecycle.apply(FoodCartCreatedEvent(command.foodCartId))
    }

    @CommandHandler
    fun handle(command: SelectProductCommand) {
        AggregateLifecycle.apply(ProductSelectedEvent(foodCartId, command.productId, command.orderId, command.quantity))
    }

    @CommandHandler
    @Throws(ProductDeselectionException::class)
    fun handle(command: DeselectProductCommand) {
        val productId: UUID = command.productId
        val quantity: Int = command.quantity
        if (!selectedProducts.containsKey(productId)) {
            throw ProductDeselectionException(
                "Cannot deselect a product which has not been selected for this Food Cart"
            )
        }
        if (selectedProducts[productId]!! - quantity < 0) {
            throw ProductDeselectionException(
                "Cannot deselect more products of ID [$productId] than have been selected initially"
            )
        }
        AggregateLifecycle.apply(ProductDeselectedEvent(foodCartId, productId, quantity))
    }

    @CommandHandler
    fun handle(command: ConfirmOrderCommand?) {
        if (confirmed) {
            logger.warn("Cannot confirm a Food Cart order which is already confirmed")
            return
        }
        AggregateLifecycle.apply(OrderConfirmedEvent(foodCartId))
    }

    @EventSourcingHandler
    fun on(event: FoodCartCreatedEvent) {
        foodCartId = event.foodCartId
        selectedProducts = HashMap()
        confirmed = false
    }

    @EventSourcingHandler
    fun on(event: ProductSelectedEvent) {
        selectedProducts.merge(
            event.productId,
            event.quantity
        ) { a: Int?, b: Int? ->
            Integer.sum(
                a!!, b!!
            )
        }
    }

    @EventSourcingHandler
    fun on(event: ProductDeselectedEvent) {
        selectedProducts.computeIfPresent(
            event.productId,
            { _: UUID?, quantity: Int -> quantity - event.quantity }
        )
    }

    @EventSourcingHandler
    fun on(event: OrderConfirmedEvent?) {
        confirmed = true
    }

    companion object {
        private val logger = LoggerFactory.getLogger(FoodCart::class.java)
    }
}