package vorabei.stock.exchange.stock.projections

import org.axonframework.config.Configuration
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.EventHandler
import org.axonframework.queryhandling.QueryHandler
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture
import org.axonframework.eventhandling.TrackingEventProcessor

import org.axonframework.config.EventProcessingConfiguration
import vorabei.stock.exchange.stock.api.*


@Component
@ProcessingGroup("product-projection")
internal class ProductProjector(private val productViewRepository: ProductViewRepository) {
    @EventHandler
    fun on(event: CreateProductEvent) {
        val foodCartView = ProductView(event.productId, event.name, event.code, event.quantity)
        productViewRepository.save(foodCartView).block()
    }

    @EventHandler
    fun on(event: BookedProductEvent) {
        productViewRepository
            .findById(event.productId)
            .map { it.quantity -= event.quantity; it }
            .flatMap { productViewRepository.save(it)}
            .block()
    }

    @EventHandler
    fun on(event: UnBookedProductEvent) {
        productViewRepository
            .findById(event.productId)
            .map { it.quantity += event.quantity; it }
            .flatMap { productViewRepository.save(it)}
            .block()
    }

    @QueryHandler
    fun handle(query: FindProductsQuery): CompletableFuture<List<ProductView>> {
        return productViewRepository
            .findAll()
            .collectList()
            .toFuture()
    }
    fun reset(config: Configuration) {
        val eventProcessingConfig: EventProcessingConfiguration = config.eventProcessingConfiguration()
        eventProcessingConfig.eventProcessor("food-cart-projection", TrackingEventProcessor::class.java)
            .ifPresent { processor: TrackingEventProcessor ->
                processor.shutDown()
                processor.resetTokens()
                processor.start()
            }
    }
}