package vorabei.stock.exchange.stock.projections

import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import java.util.*
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version

@Document
data class ProductView(
    @Id val productId: UUID,
    val name: String,
    val code: String,
    var quantity: Int,
) {

    @CreatedDate
    var createdAt: Date? = null

    @LastModifiedDate
    var modifiedAt: Date? = null

    @Version
    var version: Long? = null

}

@Repository
interface ProductViewRepository : ReactiveCrudRepository<ProductView, UUID>