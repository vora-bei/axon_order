package vorabei.stock.exchange.stock.projections

import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import java.util.*
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version

@Document
data class FoodCartView(
    @Id val foodCartId: UUID,
    val products: MutableMap<UUID, Int>
) {

    /**
     * Adds the given [productId] with the given [amount] to the map of [products].
     * Does this by performing [MutableMap.compute], where the second parameter is a bi-function.
     * The [productId] is not important for the bi-function, hence it's replaced by `_`.
     * Lastly, the `quantity` field in the bi-function is nullable, thus explaining why the elvis operator is in place.
     */
    fun addProducts(productId: UUID, amount: Int): FoodCartView {
        products.compute(productId) { _, quantity -> (quantity ?: 0) + amount }
        return this;
    }


    /**
     * Removes the specified [amount] of the given [productId] from the [products] map.
     * Does this by performing [MutableMap.compute], where the second parameter is a bi-function.
     * The [productId] is not important for the bi-function, hence it's replaced by `_`.
     * Lastly, the `quantity` field in the bi-function is nullable, thus explaining why the elvis operator is in place.
     *
     * If the left over quantity is zero, the product will be completely removed from the map.
     */
    fun removeProducts(productId: UUID, amount: Int): FoodCartView {
        val leftOverQuantity = products.compute(productId) { _, quantity -> (quantity ?: 0) - amount }
        if (leftOverQuantity == 0) {
            products.remove(productId)
        }
        return this;
    }

    @CreatedDate
    var createdAt: Date? = null

    @LastModifiedDate
    var modifiedAt: Date? = null

    @Version
    var version: Long? = null

}

@Repository
interface FoodCartViewRepository : ReactiveCrudRepository<FoodCartView, UUID>