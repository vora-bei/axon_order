package vorabei.stock.exchange.stock.projections

import org.axonframework.config.Configuration
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.AllowReplay
import vorabei.stock.exchange.stock.api.FoodCartCreatedEvent
import org.axonframework.eventhandling.EventHandler
import vorabei.stock.exchange.stock.api.ProductSelectedEvent
import vorabei.stock.exchange.stock.api.ProductDeselectedEvent
import org.axonframework.queryhandling.QueryHandler
import org.springframework.stereotype.Component
import vorabei.stock.exchange.stock.api.FindFoodCartQuery
import java.util.concurrent.CompletableFuture
import org.axonframework.eventhandling.TrackingEventProcessor

import org.axonframework.config.EventProcessingConfiguration




@Component
@AllowReplay
@ProcessingGroup("food-cart-projection")
internal class FoodCartProjector(private val foodCartViewRepository: FoodCartViewRepository) {
    @EventHandler
    fun on(event: FoodCartCreatedEvent) {
        val foodCartView = FoodCartView(event.foodCartId, mutableMapOf())
        foodCartViewRepository.save(foodCartView).block()
    }

    @EventHandler
    fun on(event: ProductSelectedEvent) {
        foodCartViewRepository
            .findById(event.foodCartId)
            .map { it.addProducts(event.productId, event.quantity) }
            .flatMap { foodCartViewRepository.save(it)}
            .block()
    }

    @EventHandler
    fun on(event: ProductDeselectedEvent) {
        foodCartViewRepository
            .findById(event.foodCartId)
            .map { it.removeProducts(event.productId, event.quantity) }
            .flatMap { foodCartViewRepository.save(it)}
            .block()
    }

    @QueryHandler
    fun handle(query: FindFoodCartQuery): CompletableFuture<FoodCartView> {
        return foodCartViewRepository
            .findById(query.foodCartId)
            .toFuture()
    }
    fun reset(config: Configuration) {
        val eventProcessingConfig: EventProcessingConfiguration = config.eventProcessingConfiguration()
        eventProcessingConfig.eventProcessor("food-cart-projection", TrackingEventProcessor::class.java)
            .ifPresent { processor: TrackingEventProcessor ->
                processor.shutDown()
                processor.resetTokens()
                processor.start()
            }
    }
}