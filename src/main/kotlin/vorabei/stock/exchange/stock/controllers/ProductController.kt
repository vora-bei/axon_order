package vorabei.stock.exchange.stock.controllers

import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.AllowReplay
import org.axonframework.eventhandling.EventHandler
import org.axonframework.extensions.reactor.commandhandling.gateway.ReactorCommandGateway
import org.axonframework.extensions.reactor.queryhandling.gateway.ReactorQueryGateway
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import reactor.core.publisher.Mono
import vorabei.stock.exchange.stock.api.*
import vorabei.stock.exchange.stock.projections.ProductView
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

@RequestMapping("/product")
@RestController
@AllowReplay
@ProcessingGroup("product-controller-projection")
internal class ProductController(
    private val commandGateway: ReactorCommandGateway,
    private val queryGateway: ReactorQueryGateway
) {
    private val codes: ConcurrentMap<String, String> = ConcurrentHashMap();

    @EventHandler
    fun onCreate(event: CreateProductEvent){
        codes.putIfAbsent(event.code, event.name);
    }

    @GetMapping("/create/code/{code}/name/{name}/quantity/{quantity}")
    fun createFoodCart(
        @PathVariable("code") code: String,
        @PathVariable("name") name: String,
        @PathVariable("quantity") quantity: Int
    ): Mono<UUID> {
        if(codes.containsKey(code))
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Not unique code", Error("Not unique code"))
        return commandGateway.send(
            CreateProductCommand(
                UUID.randomUUID(),
                name = name,
                code = code,
                quantity
            )
        )
    }

    @GetMapping("/")
    fun all(): Mono<List<ProductView>> {
        return queryGateway.query<List<ProductView>, Any>(
                FindProductsQuery(),
                ResponseTypes.multipleInstancesOf(ProductView::class.java)
            )
    }
}