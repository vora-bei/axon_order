package vorabei.stock.exchange.stock.controllers

import vorabei.stock.exchange.stock.projections.FoodCartView
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.extensions.reactor.commandhandling.gateway.ReactorCommandGateway
import org.axonframework.extensions.reactor.queryhandling.gateway.ReactorQueryGateway
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.axonframework.queryhandling.QueryGateway
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import vorabei.stock.exchange.stock.api.*
import java.util.*
import java.util.concurrent.CompletableFuture
import org.springframework.http.HttpStatus

import org.springframework.web.bind.annotation.GetMapping




@RequestMapping("/foodCart")
@RestController
internal class FoodOrderingController(private val commandGateway: ReactorCommandGateway,
                                      private val queryGateway: ReactorQueryGateway
) {
    @GetMapping(value = ["/bad-request"])
    fun badRequest(): Mono<String?>? {
        return Mono.error(IllegalArgumentException())
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "all error")
    @ExceptionHandler(Exception::class)
    fun illegalArgumentHandler(e: Exception): Mono<Exception> {
        return Mono.just(e);
    }
    @GetMapping("/create")
    fun createFoodCart(): Mono<UUID> {
        return commandGateway.send(CreateFoodCartCommand(UUID.randomUUID()))
    }

    @GetMapping("/{foodCartId}/select/{productId}/quantity/{quantity}")
    fun selectProduct(
        @PathVariable("foodCartId") foodCartId: String?,
        @PathVariable("productId") productId: String?,
        @PathVariable("quantity") quantity: Int?
    ): Mono<Any> {
        return commandGateway.send(
            CreateOrderCommand(
                UUID.fromString(productId),
                UUID.fromString(foodCartId),
                UUID.randomUUID(),
                quantity!!
            )
        )
    }

    @GetMapping("/{foodCartId}/deselect/{productId}/quantity/{quantity}")
    fun deselectProduct(
        @PathVariable("foodCartId") foodCartId: String?,
        @PathVariable("productId") productId: String?,
        @PathVariable("quantity") quantity: Int?
    ): Mono<Any> {
        return commandGateway.send<Any>(
            DeselectProductCommand(
                UUID.fromString(foodCartId), UUID.fromString(productId), quantity!!
            )
        )
    }

    @GetMapping("/{foodCartId}")
    fun findFoodCart(@PathVariable("foodCartId") foodCartId: String?): Mono<FoodCartView> {
        return queryGateway.query<FoodCartView, Any>(
            FindFoodCartQuery(UUID.fromString(foodCartId)),
            ResponseTypes.instanceOf(FoodCartView::class.java)
        )
    }
}