package vorabei.stock.exchange.stock

import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoClients
import org.axonframework.config.ConfigurationScopeAwareProvider
import org.axonframework.deadline.DeadlineManager
import org.axonframework.deadline.SimpleDeadlineManager
import org.axonframework.spring.config.AxonConfiguration
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine

import org.axonframework.eventsourcing.eventstore.EventStorageEngine




@Configuration
class Config {
    @Value("\${spring.data.mongodb.uri}")
    lateinit var uri: String;
    @Bean
    fun mongoClient(): MongoClient? {
        return MongoClients.create(uri)
    }

    @Bean
    fun eventStorageEngine(): EventStorageEngine? {
        return InMemoryEventStorageEngine()
    }

}