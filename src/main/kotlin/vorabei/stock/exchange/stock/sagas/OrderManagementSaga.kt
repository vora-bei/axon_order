package vorabei.stock.exchange.stock.sagas

import org.springframework.beans.factory.annotation.Autowired
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.deadline.DeadlineManager
import org.axonframework.modelling.saga.StartSaga
import org.axonframework.modelling.saga.SagaEventHandler
import org.axonframework.modelling.saga.SagaLifecycle
import org.axonframework.spring.stereotype.Saga
import vorabei.stock.exchange.stock.api.*
import java.lang.Error
import java.util.*
import org.axonframework.deadline.annotation.DeadlineHandler
import java.time.Duration


@Saga
internal class OrderManagementSaga {
    private lateinit var foodCartId: UUID;
    private lateinit var productId: UUID;
    private lateinit var orderId: UUID;
    private var quantity: Int = 0;
    @Autowired
    private lateinit var commandGateway: CommandGateway
    @Autowired
    private lateinit var deadlineManager: DeadlineManager
    @StartSaga
    @SagaEventHandler(associationProperty = "orderId")
    fun handle(event: CreateOrderEvent) {
        foodCartId = event.foodCartId
        quantity = event.quantity
        productId = event.productId
        orderId = event.orderId
        val deadlineId: String = deadlineManager.schedule(
            Duration.ofMillis(3000), "my-deadline"
        )
        commandGateway.sendAndWait<Any>(
            BookedProductCommand(
                event.productId,
                event.orderId,
                event.quantity
            )
        )
    }

    @SagaEventHandler(associationProperty = "orderId")
    fun handle(event: BookedProductEvent) {
        try {
            commandGateway.sendAndWait<Any>(
                SelectProductCommand(
                    foodCartId,
                    event.productId,
                    event.orderId,
                    event.quantity
                )
            )
        } catch (e: Error){
            compensateBookedProduct(event)
        }
    }

    private fun compensateBookedProduct(event: BookedProductEvent) {
        try {
            commandGateway.send<Any>(
                UnBookedProductCommand(
                    event.productId,
                    event.orderId,
                    event.quantity
                )
            )
        }  catch (e: Error){
            println(e.stackTrace)
        }
    }

    @SagaEventHandler(associationProperty = "orderId")
    fun handle(event: ProductSelectedEvent) {
        commandGateway.sendAndWait<Any>(
            SuccessOrderCommand(
                event.orderId,
            )
        )
    }
    @SagaEventHandler(associationProperty = "orderId")
    fun handle(event: UnBookedProductEvent) {
        commandGateway.sendAndWait<Any>(
            CancelOrderCommand(
                event.orderId,
                "Not enough product"
            )
        )
    }
    @SagaEventHandler(associationProperty = "orderId")
    fun handle(event: SuccessOrderEvent) {
        println("Saga N ${event.orderId} success ended")
        SagaLifecycle.end()
    }
    @SagaEventHandler(associationProperty = "orderId")
    fun handle(event: CancelOrderEvent) {
        println(event.reasonFailed)
        SagaLifecycle.end()
    }

    @DeadlineHandler(deadlineName = "my-deadline")
    fun on() {
        commandGateway.send<Any>(
            UnBookedProductCommand(
                productId,
                orderId,
                quantity
            )
        )
    }
}